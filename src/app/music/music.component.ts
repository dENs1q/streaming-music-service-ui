import {Component, Input, OnInit} from '@angular/core';
import {MusicService} from '../services/music.service';
import {Track} from 'ngx-audio-player';

@Component({
  selector: 'app-music',
  templateUrl: './music.component.html',
  styleUrls: ['./music.component.css']
})
export class MusicComponent implements OnInit {

  @Input() playlist: Track[] = [];

  constructor(public musicService: MusicService) {
  }

  ngOnInit(): void {
    this.musicService.getAllMusic().subscribe(data =>
      data.map(t => {
        const track = new Track();
        track.title = t.name;
        track.link = `${(this.musicService.host)}/api/track/${t.id}/download`;
        this.playlist.push(track);
        return track;
      }));
  }


}
