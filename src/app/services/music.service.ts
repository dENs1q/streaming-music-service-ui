import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AudioTrack} from '../interfaces/AudioTrack';


@Injectable({providedIn: 'root'})
export class MusicService {

  constructor(private http: HttpClient) {
  }

  host = 'http://localhost:9200';

  getAllMusic(): Observable<AudioTrack[]> {
    return this.http.get<AudioTrack[]>(`${(this.host)}/api/track`)
      .pipe();
  }
}
