export interface AudioTrack {
  id: number;
  name: string;
  fileName: string;
  created?: any;
  author?: any;
  album?: any;
  genre?: string;
}
